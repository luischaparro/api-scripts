# Introduction #

API helper scripts to ease developing and testing.

Quick Start
---------------

**Install dependencies and run application**

```shell
$ npm install
$ npm start
```
How to use?
---------------
1. Select your script
2. Select your phdapi environment
3. Select a Web2 API Key. (Headers get managed accordingly)
4. Enter account credentials or a JWT auth token depending on the api key selected. JWT token can also get auto-retrieved

Good to know
---------------
* As of Feb 10th, saving a favorite order only works with the Web1 API Key because generating a cartId fails with the Web2 key
* When using Web2 generate a JWT token through the app by signing in and copying the UA_token from cookies
* Retrieving a JWT auth token only does it for _just.peachy-staging@staypeachy.com_. Update data.js to get it for another account