const {
  click,
  write,
  into,
  textBox,
  text,
  goto,
  waitFor,
  openBrowser,
  setCookie,
  closeBrowser,
  button,
  getCookies,
  $,
} = require('taiko');
const { loginBaseData } = require('../data');

const getJwtUaToken = async () => {
  try {
    console.log('Setting up headless browser...');
    const url = 'https://leda.pizzahut.com/?auth=1';;
    const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4270.0 Safari/537.36';
    await openBrowser({ args: [`--user-agent=${userAgent}`, `--no-sandbox`] });
    await setCookie('SEGNUM', '0', { url });
    await setCookie('9a3d48f2292d7083178ebfc7720beb9260588988dd10ff7e', JSON.stringify({ override_universal_recaptcha: '0'}), { url, domain: '.pizzahut.com' });
    await setCookie('akacookie', 'byOGh4aAEilg63nT', { url, domain: '.pizzahut.com' });
    await goto(url, { navigationTimeout: 300000 });

    console.log(`Retrieving token for ${loginBaseData.username}`);
    await waitFor(async () => $('[data-testid="login_rail_iframe"]').exists());
    await write(loginBaseData.username, into(textBox('Email')), { delay: 100 });
    await write(loginBaseData.password, into(textBox('Password')), { delay: 100 });
    await click(button('Sign in'));
    await text(/hi, [A-Za-z]/i).exists();

    const cookies = await getCookies();
    const { value: uaToken } = cookies.find(cookie => cookie.name === 'UA_token');

    if (!uaToken) throw new Error('No UA_token found!');

    console.log('\n====================================');
    console.log(`Token retrieved: ${uaToken}`);
    console.log('====================================\n');
    return uaToken;
  } catch (error) {
    throw new Error(error);
  } finally {
    await closeBrowser();
  }
}

module.exports = {
  getJwtUaToken,
};