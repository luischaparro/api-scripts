const { tokenizePayment } = require('../common/helpers');
const { creditCards, tpgcCards, giftCards } = require('../data');

const _getCustomerPayments = async (phdAPI) => {
  console.log('Retrieving customer payments...');
  try {
    const { data: customerInfo } = await phdAPI.get('/customer');
    console.log('Customer\'s payments: ', customerInfo.payments.length);
    return customerInfo.payments;
  } catch (error) {
    throw new Error(error);
  };
};

const _deleteAllPayments = async (phdAPI, payments) => {
  console.log(`Deleting ${payments.length} payments...`);

  try {
    for (const payment of payments) {
      await phdAPI.delete(`/customer/payments/${payment.payment_id}/${payment.type}`);
    }
  } catch (error) {
    throw new Error(error);
  }
};

const _generatemask = (cardMask) => cardMask
  .split('')
  .map((char, i) => i > 5 && i < 12 ? '*' : char)
  .join('');

const _addCard = async (phPay, numberOfCards, cardType) => {
  console.log(`Preparing ${cardType} Card Payment...`);
  const isCredit = cardType === 'Credit';
  const cards = isCredit ? creditCards : tpgcCards;

  try {
    for (const index of [...Array(numberOfCards).keys()]) {
      const cardMask = cards[index].card_mask;
      const tokenizedCard = await tokenizePayment(phPay, cardMask);
      const isDefault = isCredit && index === 0;
      const cardType = isCredit ? 'creditcard' : 'tpgc';

      console.log(`Adding ${cardType} Card Payment ${index + 1}...`);
      await phPay.post('/customer/payments/tokenized', {
        is_default: isDefault,
        name: `Auto Generated ${cardType} Card #${index + 1}`,
        type: cardType,
        metadata: {
            ...cards[index],
            card_mask: _generatemask(cardMask),
            token: tokenizedCard
        }
      });
    }
  } catch (error) {
    throw new Error(error);
  }
};

const _addGiftCard = async (phPay, numberOfCards) => {
  console.log(`Preparing Gift Card Payment...`);
  
  try {
    for (const index of [...Array(numberOfCards).keys()]) {
      console.log(`Adding Gift Card Payment ${index + 1}...`);
      await phPay.post('/customer/payments/tokenized', {
        type: 'giftcard',
        metadata: giftCards[index]
      });
    }
  } catch (error) {
    throw new Error(error);
  }
};

const generatePaymentMethod = async (phdAPI, phPay) => {
  try {
    const payments = await _getCustomerPayments(phdAPI);

    if (payments.length) {
      await _deleteAllPayments(phdAPI, payments);
    }

    await _addCard(phPay, 1, 'Credit');
    await _addGiftCard(phPay, 6);
    await _addCard(phPay, 4, 'TPGC');
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  generatePaymentMethod
}