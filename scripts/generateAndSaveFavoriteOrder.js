const { tokenizePayment } = require('../common/helpers');
const {
  loginBaseData,
  storeSearchBaseData,
  orderBaseData,
} = require('../data');

const _localize = async (phdAPI) => {
  console.log('localize');
  try {
    const authToken = phdAPI.defaults.headers['Auth-Token'];
    delete phdAPI.defaults.headers['Auth-Token'];
    const { data } = await phdAPI.post('/stores/search', storeSearchBaseData);
    phdAPI.defaults.headers['Auth-Token'] = authToken;
    return data.find(store => store.online_status === 'online').store_number;
  } catch (error) {
    const err = error.message || error;
    throw new Error(err);
  };
};
const _createCart = async (phdAPI, storeNumber) => {
  console.log(`CreateCart using storeNumber: ${storeNumber}`);
  try {
    const cartCreationData = {
      store_number: storeNumber,
      occasion_id: storeSearchBaseData.occasion_id,
    };
    const { data } = await phdAPI.post('/carts', cartCreationData);

    if (!data.cart_id) throw new Error('No cartId created...');

    return data.cart_id;
  } catch (error) {
    const err = error.message || error;
    throw new Error(err);
  };
};
const _populateCart = async (phdAPI, cartID, storeNumber) => {
  console.log(`PopulateCart using storeNumber: ${storeNumber} and cartID: ${cartID}`);
  try {
    const cartProducts = {
      products: Array(3).fill(
        {
          product_id: `11131~P^B~${storeNumber}`,
          quantity: 5,
          size_id: 'P~L',
          modifiers: [
              {
                  modifier_id: `C~H~${storeNumber}`,
                  quantities: [
                      13
                  ]
              },
              {
                  modifier_id: `S~PS~${storeNumber}`,
                  quantities: [
                      1
                  ]
              },
              {
                  modifier_id: `V~BO~${storeNumber}`,
                  quantities: [
                      2,
                      1
                  ]
              }
          ]
        }
      ),
    };
    await phdAPI.post(`/carts/${cartID}/products/multi`, cartProducts);
  } catch (error) {
    const err = error.message || error;
    throw new Error(err);
  };
};

const _sendOrder = async (phPay, cartID, cardToken) => {
  console.log(`SendOrder using cartToken: ${cardToken} and cartID: ${cartID}`);
  try {
    await phPay.post('/orders/tokenized', {
      cart_id: cartID,
      orderBaseData,
      customer: {
        ...orderBaseData.customer,
          email: loginBaseData.username,
      },
      forms_of_payment: [
          {
              ...orderBaseData.forms_of_payment[0],
              metadata: {
                ...orderBaseData.forms_of_payment[0].metadata,
                  token: cardToken
              }
          }
      ]
    });
  } catch (error) {
    const err = error.message || error;
    throw new Error(err);
  };
};

const _saveAsFavorite = async (phdAPI) => {
  console.log('Saving As Favorite Order');
  try {
    await phdAPI.post('/favorites', {
      is_default: false,
      name: `Auto Order #${Math.floor(Math.random()*(100-1+1)+1)}`,
      form_of_payment: {
          type: 'cash'
      }
    });
  } catch (error) {
    const err = error.message || error;
    throw new Error(err);
  };
};

const _clearCart = async (phdAPI, cartID) => {
  console.log(`ClearCart using cartID: ${cartID}`);
  try {
    await phdAPI.post(`/carts/${cartID}/clear`);
  } catch (error) {
    if (error?.response?.data?.statusCode !== '200') {
      const err = error.message || error;
      throw new Error(err);
    }
  };
}

const generateAndSaveFavoriteOrder = async (phdAPI) => {
  console.log('Creating Order and Saving as favorite...');
  let cartID = null;
  try {
    // Localize
    const storeNumber = await _localize(phdAPI);
    // Create Cart
    cartID = await _createCart(phdAPI, storeNumber);
    // Populate Cart
    await _populateCart(phdAPI, cartID, storeNumber);
    // Save As Favorite
    await _saveAsFavorite(phdAPI);
  } catch (error) {
    console.log(error);
  } finally {
    // Clear cart
    if (cartID)
      await _clearCart(phdAPI, cartID);
  };
};

module.exports = {
  generateAndSaveFavoriteOrder
};