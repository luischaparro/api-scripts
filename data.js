const loginBaseData = {
  username: 'just.peachy-staging@staypeachy.com',
  password: 'JustPeachy',
  channel_id: 'Web2'
};

const creditCards = [
  {
    type: 'VISA',
    card_mask: '4012000088888886',
    card_code: '249',
    postal_code: '60654',
    expiration: '01/30',
    gateway: 'VERIFONE',
  },
  {
    type: 'MC',
    card_mask: '5424180279791740',
    card_code: '249',
    postal_code: '60654',
    expiration: '01/30',
    gateway: 'VERIFONE',
  }
];

const tpgcCards = [
  {
    type: 'MC',
    card_mask: '4436130009201980',
    card_code: '249',
    expiration: '01/27',
    gateway: 'VERIFONE',
  },
  {
    type: 'MC',
    card_mask: '4436130008040595',
    card_code: '672',
    expiration: '01/27',
    gateway: 'VERIFONE',
  },
  {
    type: 'MC',
    card_mask: '4436130005554481',
    card_code: '678',
    expiration: '01/27',
    gateway: 'VERIFONE',
  },
  {
    type: 'MC',
    card_mask: '4436130005764510',
    card_code: '886',
    expiration: '01/27',
    gateway: 'VERIFONE',
  },

];

const giftCards = [
  {
    number: '611062578495',
    pin: '8619',
  },
  {
    number: '612120525731',
    pin: '4231',
  },
  {
    number: '614953129753',
    pin: '4859',
  },
  {
    number: '612120525734',
    pin: '7734',
  },
  {
    number: '612120525733',
    pin: '3960',
  },
  {
    number: '612120525732',
    pin: '3812',
  },
];

const storeSearchBaseData = {
  postal_code: '60657',
  limit: 0,
  occasion_id: 'CARRYOUT'
};

const orderBaseData = {
  customer: {
      first_name: 'Peachy',
      last_name: 'Dev',
      coppa_agreement: true,
      verified_age_gate: true,
      phone: '214-111-2222',
      phone_extension: ''
  },
  delivery_instructions: 'Ring',
  gratuity: 0,
  forms_of_payment: [
      {
          type: 'creditcard',
          name: 'Peachy Card',
          metadata: { ...creditCards[0] }
      }
  ]
};

module.exports = {
  loginBaseData,
  storeSearchBaseData,
  orderBaseData,
  creditCards,
  tpgcCards,
  giftCards,
};