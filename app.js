const prompts = require('prompts');
const axios = require('axios');
const { login } = require('./common/helpers');
const { generatePaymentMethod } = require('./scripts/generatePaymentMethods');
const { generateAndSaveFavoriteOrder } = require('./scripts/generateAndSaveFavoriteOrder');
const { loginBaseData } = require('./data');

const _getAxiosInstance = (env, key) => ({
  phdApi: axios.create({
    baseURL: `https://services-${env}.digital.pizzahut.com/phdapi/v1`,
    params: { key },
    headers: { akaatornr: 'X6mgXChU4Ssu6rmF', ConversationID: '123456789', 'X-Server-Env': env === 'dev' ? 'leda' : 'metis', 'Auth-Channel': 'WEB2' }
  }),
  phPay: axios.create({
    baseURL: `https://services-${env}.digital.pizzahut.com/phpay/v1`,
    params: { key },
    headers: { akaatornr: 'X6mgXChU4Ssu6rmF', ConversationID: '123456789', 'X-Server-Env': env === 'dev' ? 'leda' : 'metis', 'Auth-Channel': 'WEB2' }
  })
});

const WEB1Key = 'RyAzYIiBQMB6ISN8CAOiulwmGHA68F4c';
const WEB2Key = 'ApXqGyM6xPAp0AXTYIi3DSGGBGeRDXSS';
const _isWeb1 = (key) => key === WEB1Key;
const _isWeb2 = (key) => key === WEB2Key;

const questions = [
  {
    name: 'script',
    message: 'Select script to run (Only PHDAPI V1 is supported):',
    type: 'select',
    choices: [
      { title: 'Generate Payment Methods', value: 'GeneratePaymentMethods' },
      { title: 'Create Favorite Order', value: 'FavoriteOrder' },
      // { title: 'Get JWT UA_token', value: 'GetJwtUAToken' },
    ]
  },
  {
    name: 'environment',
    message: 'Select environment:',
    type: 'select',
    choices: [
      { title: 'stg', value: 'staging' },
      { title: 'dev', value: 'dev' },
    ]
  },
  {
    name: 'apiKey',
    message: 'Select api key to use:',
    type: 'select',
    choices: [
      { title: 'Web2', value: WEB2Key },
      { title: 'Web1', value: WEB1Key },
    ]
  },
  {
    name: 'email',
    initial: loginBaseData.username,
    message: 'Enter account email to use:',
    type: (_, { apiKey }) => _isWeb1(apiKey) ? 'text' : null
  },
  {
    name: 'password',
    initial: loginBaseData.password,
    message: 'Enter account password to use:',
    type: (_, { apiKey }) => _isWeb1(apiKey) ? 'password' : null
  },
  {
    name: 'jwtToken',
    message: 'Enter JWT Auth Token or hit enter to retrieve one:',
    initial: 'retrieve',
    type: (_, { apiKey }) => _isWeb2(apiKey) ? 'text' : null
  }
];

(async () => {
  try {
    const { environment, apiKey, script, ...answers } = await prompts(questions);
  
    const { phdApi, phPay } = _getAxiosInstance(environment, apiKey);

    const authToken = await login(phdApi, answers);

    phdApi.defaults.headers['Auth-Token'] = authToken;
    phPay.defaults.headers['Auth-Token'] = authToken;
  
    if (script === 'FavoriteOrder') 
      generateAndSaveFavoriteOrder(phdApi);
    else if (script === 'GeneratePaymentMethods')
      generatePaymentMethod(phdApi, phPay);
  } catch (error) {
    console.log(error); 
  }
})();