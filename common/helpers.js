const { getJwtUaToken } = require("../scripts/getJwtUaToken");

const login = async (phdAPI, { email: username, password, jwtToken }) => {
  console.log('Loging');
  
  try {
    if (jwtToken === 'retrieve') return await getJwtUaToken();
    if (jwtToken) return jwtToken;
    if (!jwtToken && (!username || !password)) throw new Error('Missing params at login.');

    const { data } = await phdAPI.post('/customer/login', { username, password, channel_id: 'WEB2' });
    return data.auth_token;
  } catch (error) {
    throw new Error(error);
  };
};

const tokenizePayment = async (phPay, cardNumber) => {
  console.log(`Tokenizing Card ${cardNumber}...`);
  try {
    const { data } = await phPay.post('/tokenize', {
      expiration_month: 1,
      expiration_year: 30,
      pan: cardNumber
    });

    return data.token;
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  login,
  tokenizePayment,
};